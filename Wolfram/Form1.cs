﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Wolfram.NETLink;

namespace Wolfram
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private IKernelLink kernelLink;

        private void button1_Click(object sender, EventArgs e)
        {
            mathKernel1.Compute(
String.Format(@"Get[""{0}{1}""]", AppDomain.CurrentDomain.BaseDirectory, textBox2.Text).Replace("\\", "\\\\") + @"

ks = 4;
is = 6;

Subscript[U, 1] = {{1, 3}, {2, 1}, {2, 6}, {3, 4}, {3, 6}, {4, 6}, {5, 4}, {6, 5}};
Subscript[U, 2] = {{1, 3}, {1, 4}, {1, 5}, {1, 6}, {2, 6}, {3, 2}, {3, 4}, {4, 6}};
Subscript[U, 3] = {{1, 3}, {1, 5}, {2, 1}, {2, 6}, {4, 6}, {5, 2}, {5, 4}, {6, 5}};
Subscript[U, 4] = {{1, 4}, {1, 5}, {1, 6}, {2, 1}, {2, 6}, {3, 2}, {3, 4}, {3, 6}, {4, 6}, {5, 2}, {5, 4}};

Subscript[Ut, 1] = {{2, 1}, {2, 6}, {3, 4}, {4, 6}, {5, 4}};
Subscript[Ut, 2] = {{1, 3}, {1, 5}, {1, 6}, {2, 6}, {4, 6}};
Subscript[Ut, 3] = {{1, 3}, {1, 5}, {2, 6}, {4, 6}, {5, 2}};
Subscript[Ut, 4] = {{1, 4}, {1, 5}, {2, 6}, {3, 2}, {4, 6}};

Subscript[Uc, 1] = {};
Subscript[Uc, 2] = {{3, 4}};
Subscript[Uc, 3] = {{2, 1}, {5, 4}};
Subscript[Uc, 4] = {{3, 4}, {5, 2}};

Subscript[a, 1] = {-3, 16, 9, -11, 3, -14};
Subscript[a, 2] = {26, 2, 6, -5, -6, -23};
Subscript[a, 3] = {7, 4, -1, -7, -2, -1};
Subscript[a, 4] = {5, 8, 11, -15, -2, -7};

leftParts = {
   4 Subscript[x[1], 1, 3] + 2 Subscript[x[1], 2, 1] + 
    6 Subscript[x[1], 2, 6] + 7 Subscript[x[1], 3, 4] + 
    3 Subscript[x[1], 3, 6] + 4 Subscript[x[1], 4, 6] + 
    5 Subscript[x[1], 5, 4] + 6 Subscript[x[1], 6, 5] + 
    3 Subscript[x[2], 1, 3] + 7 Subscript[x[2], 1, 4] + 
    2 Subscript[x[2], 1, 5] + 3 Subscript[x[2], 1, 6] + 
    7 Subscript[x[2], 2, 6] + 7 Subscript[x[2], 3, 2] + 
    10 Subscript[x[2], 3, 4] + 9 Subscript[x[2], 4, 6] + 
    7 Subscript[x[3], 1, 3] + 5 Subscript[x[3], 1, 5] + 
    7 Subscript[x[3], 2, 1] + Subscript[x[3], 2, 6] + 
    10 Subscript[x[3], 4, 6] + 3 Subscript[x[3], 5, 2] + 
    6 Subscript[x[3], 5, 4] + 4 Subscript[x[3], 6, 5] + 
    8 Subscript[x[4], 1, 4] + 8 Subscript[x[4], 1, 5] + 
    2 Subscript[x[4], 1, 6] + 8 Subscript[x[4], 2, 1] + 
    Subscript[x[4], 2, 6] + 2 Subscript[x[4], 3, 2] + 
    2 Subscript[x[4], 3, 4] + 2 Subscript[x[4], 3, 6] + 
    9 Subscript[x[4], 4, 6] + Subscript[x[4], 5, 4],
   
   3 Subscript[x[1], 1, 3] + 7 Subscript[x[1], 2, 1] + 
    7 Subscript[x[1], 2, 6] + 3 Subscript[x[1], 3, 4] + 
    2 Subscript[x[1], 3, 6] + 5 Subscript[x[1], 4, 6] + 
    6 Subscript[x[1], 5, 4] + 8 Subscript[x[1], 6, 5] + 
    9 Subscript[x[2], 1, 3] + 6 Subscript[x[2], 1, 4] + 
    7 Subscript[x[2], 1, 5] + 8 Subscript[x[2], 1, 6] + 
    7 Subscript[x[2], 2, 6] + 8 Subscript[x[2], 3, 2] + 
    8 Subscript[x[2], 3, 4] + 3 Subscript[x[2], 4, 6] + 
    5 Subscript[x[3], 1, 3] + 9 Subscript[x[3], 1, 5] + 
    4 Subscript[x[3], 2, 1] + 7 Subscript[x[3], 2, 6] + 
    8 Subscript[x[3], 4, 6] + 4 Subscript[x[3], 5, 2] + 
    3 Subscript[x[3], 5, 4] + 2 Subscript[x[3], 6, 5] + 
    4 Subscript[x[4], 1, 4] + 2 Subscript[x[4], 1, 5] + 
    4 Subscript[x[4], 1, 6] + 8 Subscript[x[4], 2, 1] + 
    7 Subscript[x[4], 2, 6] + 10 Subscript[x[4], 3, 2] + 
    3 Subscript[x[4], 3, 4] + 2 Subscript[x[4], 3, 6] + 
    2 Subscript[x[4], 4, 6] + 2 Subscript[x[4], 5, 2] + 
    7 Subscript[x[4], 5, 4],
   
   Subscript[x[1], 1, 3] + Subscript[x[2], 1, 3] + Subscript[x[3], 1, 3],
   Subscript[x[2], 1, 5] + Subscript[x[3], 1, 5] + Subscript[x[4], 1, 5],
   Subscript[x[1], 5, 4] + Subscript[x[3], 5, 4] + Subscript[x[4], 5, 4]
   };

rightParts = {853, 908, 11, 17, 15};

SolveLabs23[x, y, ks, is, U, Ut, Uc, a, leftParts, rightParts];");

            var text = new StringBuilder();
            text.Append("results");
            foreach (var s in mathKernel1.PrintOutput)
            {
                text.Append(s);
            }
            textBox1.Text = text.ToString();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            kernelLink = MathLinkFactory.CreateKernelLink();
            kernelLink.WaitAndDiscardAnswer();
            var command = new StringBuilder();

            int[][][] U = {
                new [] {new []{1, 3}, new []{2, 1}, new []{2, 6}, new []{3, 4}, new []{3, 6}, new []{4, 6}, new []{5, 4}, new []{6, 5}},
                new [] {new []{1, 3}, new []{1, 4}, new []{1, 5}, new []{1, 6}, new []{2, 6}, new []{3, 2}, new []{3, 4}, new []{4, 6}},
                new [] {new []{1, 3}, new []{1, 5}, new []{2, 1}, new []{2, 6}, new []{4, 6}, new []{5, 2}, new []{5, 4}, new []{6, 5}},
                new [] {new []{1, 4}, new []{1, 5}, new []{1, 6}, new []{2, 1}, new []{2, 6}, new []{3, 2}, new []{3, 4}, new []{3, 6}, new []{4, 6}, new []{5, 2}, new []{5, 4}}
            };

            int[][][] lambda = {
                new [] {new []{ 4, 2, 6, 7, 3, 4, 5, 6 }, new []{ 3, 7, 7, 3, 2, 5, 6, 8 } },
                new [] {new []{ 3, 7, 2, 3, 7, 7, 10, 9 }, new []{ 9, 6, 7, 8, 7, 8, 8, 3 } },
                new [] {new []{ 7, 5, 7, 1, 10, 3, 6, 4 }, new []{ 5, 9, 4, 7, 8, 4, 3, 2 } },
                new [] {new []{ 8, 8, 2, 8, 1, 2, 2, 2, 9, 0, 1 }, new []{ 4, 2, 4, 8, 7, 10, 3, 2, 2, 2, 7 } }
            };

            int[][] z = {
                new [] {1, 1, 1, 0},
                new [] {0, 1, 1, 1},
                new [] {1, 0, 1, 1}
            };

            int[][] U0 = {
                new [] {1, 3},
                new [] {1, 5},
                new [] {5, 4}
            };





            int[][][] Ut = {
                new [] {new []{2, 1}, new []{2, 6}, new []{3, 4}, new []{4, 6}, new []{5, 4}},
                new [] {new []{1, 3}, new []{1, 5}, new []{1, 6}, new []{2, 6}, new []{4, 6}},
                new [] {new []{1, 3}, new []{1, 5}, new []{2, 6}, new []{4, 6}, new []{5, 2}},
                new [] {new []{1, 4}, new []{1, 5}, new []{2, 6}, new []{3, 2}, new []{4, 6}}
            };

            int[][][] Uc = {
                new int[][] {},
                new [] {new []{3, 4}},
                new [] {new []{2, 1}, new []{5, 4}},
                new [] {new []{3, 4}, new []{5, 2}}
            };

            int[][] a = {
                new [] {-3, 16, 9, -11, 3, -14},
                new [] {26, 2, 6, -5, -6, -23},
                new [] {7, 4, -1, -7, -2, -1},
                new [] {5, 8, 11, -15, -2, -7}
            };


            command.Append(String.Format(@"Get[""{0}{1}""]", AppDomain.CurrentDomain.BaseDirectory, textBox2.Text).Replace("\\", "\\\\"));


            command.Append(@"ks = 4;");
            command.Append(@"is = 6;");
            command.Append(@"q = 2;");
            Expr exp;

            ILoopbackLink loop = MathLinkFactory.CreateLoopbackLink();


            loop.PutFunction("Table", 1);
            loop.Put(U);
            exp = loop.GetExpr();
            command.Append(String.Format(@"U = {0};", exp));

            loop.PutFunction("Table", 1);
            loop.Put(lambda);
            exp = loop.GetExpr();
            command.Append(String.Format(@"lambda = {0};", exp));

            loop.PutFunction("Table", 1);
            loop.Put(z);
            exp = loop.GetExpr();
            command.Append(String.Format(@"z = {0};", exp));

            loop.PutFunction("Table", 1);
            loop.Put(U0);
            exp = loop.GetExpr();
            command.Append(String.Format(@"U0 = {0};", exp));
            
            loop.PutFunction("Table", 1);
            loop.Put(Ut);
            exp = loop.GetExpr();
            command.Append(String.Format(@"Ut = {0};", exp));

            loop.PutFunction("Table", 1);
            loop.Put(Uc);
            exp = loop.GetExpr();
            command.Append(String.Format(@"Uc = {0};", exp));
            
            loop.PutFunction("Table", 1);
            loop.Put(a);
            exp = loop.GetExpr();
            command.Append(String.Format("a = {0};", exp));


            loop.PutFunction("Table", 1);
            loop.Put(new[] { 853, 908, 11, 17, 15 });
            exp = loop.GetExpr();

            command.Append(String.Format(@"alpha = {0};", exp));


            loop.Close();
            command.Append(String.Format(@"{0}[x, y, ks, is, U, Ut, Uc, U0, lambda, z, a, alpha, q];", textBox3.Text));

            kernelLink.Close();

            mathKernel1.Compute(command.ToString());
            while (mathKernel1.IsComputing)
            {
            }

            var text = new StringBuilder();
            foreach (var s in mathKernel1.Messages)
            {
                text.Append(s);
            }
            foreach (var s in mathKernel1.Messages)
            {
                text.Append(s);
            }
            textBox1.Text = text.ToString();

        }
    }
}
